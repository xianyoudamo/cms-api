/*
 * @Descripttion:
 * @version:
 * @Author: Damo
 * @Date: 2020-11-22 19:39:24
 * @LastEditors: Damo
 * @LastEditTime: 2020-11-22 20:54:43
 */
const passport = require('passport')
const localStrategy = require('passport-local').Strategy
const bearerStrategy = require('passport-http-bearer').Strategy
const helper = require('../utils/helper')
const model = require('../modules/database/schema/users')
var jwt = require("jsonwebtoken")

var jwt_config = require("config").get("jwt_config")

// 通过登录函数初始化
/**
 * 初始化 passport 框架
 *
 * @param  {[type]}   app       全局应用程序
 * @param  {[type]}   loginFunc 登录函数
 * @param  {Function} callback  回调函数
 */
module.exports.setup = function (app, loginFunc, callback) {
  // 用户名密码 登录策略
  passport.use(new localStrategy(
    function (username, password, done) {
      if (!loginFunc) return done("登录验证函数未设置")

      loginFunc(username, password, function (err, user) {
        if (err) return done(err)
        return done(null, user)
      })
    }))

  // token 验证策略
  passport.use(new bearerStrategy(
    function (token, done) {
      jwt.verify(token, jwt_config.get("secretKey"), function (err, decode) {
        if (err) {
          return done("验证错误")
        }
        return done(null, decode)
      })
    }
  ))

  // 初始化passport模块
  app.use(passport.initialize());
  // 回调函数
  callback && callback();
};

/**
 * 登录验证逻辑
 *
 * @param  {[type]}   req  请求
 * @param  {[type]}   res  响应
 * @param  {Function} next [description]
 */
module.exports.login = function (req, res, next) {

  passport.authenticate('local', async function (err, user, info) {

    if (err) return res.sendResult(null, 400, err)
    if (!user) return res.sendResult(null, 400, "参数错误")

    // 获取角色信息
    var token = jwt.sign({
      "uid": user._id,
      "rid": user.role_id
    }, jwt_config.get("secretKey"), {
      "expiresIn": jwt_config.get("expiresIn")
    })
    user.token = "Bearer " + token
    const loginStatus = {
      login_ip: helper.getClientIP(req),
      login_time: +new Date()
    }
    await model.updateOne({
      _id: user._id
    }, loginStatus)
    return res.sendResult(user, 200, '登录成功')
  })(req, res, next)

}

/**
 * token验证函数
 *
 * @param  {[type]}   req  请求对象
 * @param  {[type]}   res  响应对象
 * @param  {Function} next 传递事件函数
 */
module.exports.tokenAuth = function (req, res, next) {
  passport.authenticate('bearer', {
    session: false
  }, function (err, tokenData) {
    if (err) return res.sendResult(null, 400, '无效token')
    if (!tokenData) return res.sendResult(null, 400, '无效token')
    req.userInfo = {}
    req.userInfo._id = tokenData["uid"]
    req.userInfo.role_id = tokenData["rid"]
    next()
  })(req, res, next);
}