/*
 * @Descripttion:
 * @version:
 * @Author: Damo
 * @Date: 2020-11-22 18:03:48
 * @LastEditors: Damo
 * @LastEditTime: 2020-11-24 17:46:29
 */
const Role = require('../modules/database/curd/roles')
const filter = require('../modules/filters/roleFilter')
module.exports = {
  add: {
    post: async (req, res, next) => {
      try {
        let body = req.body
        const data = await Role.addRole(body)
        res.sendResult(data, 201, '创建成功')
      } catch (err) {
        res.sendResult(null, 404, err.message)
      }
    }
  },
  get: {
    list: (req, res, next) => {
      Role.getRoles()
        .then(data => {
          console.log(data)
          res.sendResult(data.map(item => {
            return filter.extractRole(item)
          }), 200, '角色列表获取成功')
        })
        .catch(error => res.sendResult(null, 404, error.message))
    },
    user: (req, res, next) => {
      Role.getRole(req.params.id)
        .then(data => {
          console.log(data)
          res.sendResult(filter.extractRole(data), 200, '角色获取成功')
        })
        .catch(error => res.sendResult(null, 404, '角色不存在'))
    }
  },
  put: (req, res, next) => {
    Role.putRole(req.params.id, req.body)
      .then(data => {
        console.log(data)
        if (data.n === 0) {
          res.sendResult(null, 404, '角色不存在')
        } else {
          res.sendResult(null, 200, '角色更新成功')
        }
      })
      .catch(error => res.sendResult(null, 500, error.message))
  },
  delete: (req, res, next) => {
    Role.deleteRole(req.params.id)
      .then(data => {
        console.log(data)
        if (data.n === 0) {
          res.sendResult(null, 404, '角色不存在')
        } else {
          res.sendResult(null, 204, '角色删除成功')
        }

      })
      .catch(error => res.sendResult(null, 500, error.message))
  }
}