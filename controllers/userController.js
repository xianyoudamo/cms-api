/*
 * @Descripttion:
 * @version:
 * @Author: Damo
 * @Date: 2020-11-21 21:42:32
 * @LastEditors: Damo
 * @LastEditTime: 2020-11-25 00:17:29
 */
const User = require('../modules/database/curd/users')
const helper = require('../utils/helper')
const filter = require('../modules/filters/userFilter')
const config = require('config')
module.exports = {
  add: {
    post: (req, res, next) => {
      User.addUser({
        ...req.body,
        loginip: helper.getClientIP(req)
      }).then(data => {
        // 输出的是提取的用户数据，敏感数据过滤掉
        res.sendResult(filter.extractUser(data), 201, '用户创建成功')
      }).catch(error => res.sendResult(null, 404, error.message))
    }
  },
  get: {
    list: (req, res, next) => {
      User.getUsers({
        ...req.query,
        show: config.get('pagination_show.admin')
      }).then(data => {
        const users = data.records.map(item => {
          return filter.extractUser(item)
        })
        res.sendResult({
          users,
          total: data.total,
          pagenum: data.page,
          pagesize: data.size
        }, 200, '用户列表获取成功')
      }).catch(error => res.sendResult(null, 404, error.message))
    },
    user: (req, res, next) => {
      User.getUser(req.params.id)
        .then(data => {
          // 输出的是提取的用户数据，敏感数据过滤掉
          res.sendResult(filter.extractUser(data), 200, '用户获取成功')
        }).catch(error => res.sendResult(null, 404, '用户不存在'))
    }
  },
  put: {
    user: (req, res, next) => {
      User.putUser(req.params.id, req.body)
        .then(data => {
          console.log(data);
          if (data.n === 0) {
            res.sendResult(null, 404, '用户不存在')
          } else {
            res.sendResult(null, 200, '用户信息更新成功')
          }
        }).catch(error => res.sendResult(null, 500, error.message))
    },
    state: (req, res, next) => {
      User.putUser(req.params.id, req.body)
        .then(data => {
          console.log(data);
          if (data.n === 0) {
            res.sendResult(null, 404, '用户不存在')
          } else {
            res.sendResult(null, 200, '用户状态更新成功')
          }
        }).catch(error => res.sendResult(null, 500, error.message))
    },
    role: (req, res, next) => {
      User.putUser(req.params.id, req.body)
        .then(data => {
          console.log(data);
          if (data.n=== 0) {
            res.sendResult(null, 404, '用户不存在')
          } else {
            res.sendResult(null, 200, '用户角色更新成功')
          }
        }).catch(error => res.sendResult(null, 500, error.message))
    }
  },
  delete: (req, res, next) => {
    User.deleUser(req.params.id)
      .then(data => {
        if (data.n === 0) {
          res.sendResult(null, 404, '用户不存在')
        } else {
          res.sendResult(null, 204, '用户删除成功')
        }
      }).catch(error => res.sendResult(null, 500, error.message))
  }
}