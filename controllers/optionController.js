/*
 * @Descripttion:
 * @version:
 * @Author: Damo
 * @Date: 2020-11-24 17:50:37
 * @LastEditors: Damo
 * @LastEditTime: 2020-11-25 00:18:49
 */
const Option = require('../modules/database/curd/options')
const filter = require('../modules/filters/optionFilter')
module.exports = {
  post: (req, res, next) => {
    Option.addOption(req.body)
      .then(data => {
        // 输出的是提取的用户数据，敏感数据过滤掉
        res.sendResult(filter.extractOption(data), 201, '设置创建成功')
      })
      .catch(error => res.sendResult(null, 404, error.message))
  },
  get: {
    group: (req, res, next) => {
      Option.getListByGid(req.params.gid)
        .then(data => {
          // 输出的是提取的用户数据，敏感数据过滤掉
          res.sendResult(data.map(item => {
            return filter.extractOption(item)
          }), 200, '设置列表获取成功')
        }).catch(error => res.sendResult(null, 404, '分组不存在'))
    },
    list: (req, res, next) => {
      Option.getList()
        .then(data => {
          // 输出的是提取的用户数据，敏感数据过滤掉
          const result = data.map(item => {
            return filter.extractOption(item)
          })
          if (req.query.type === 'tree') {
            const tree = {}
            result.forEach(item => {
              if (!tree.hasOwnProperty(item.group_id)) {
                tree[item.group_id] = []
              }
              tree[item.group_id].push(item)
            })
            res.sendResult(tree, 200, '设置列表获取成功')
          } else {
            res.sendResult(result, 200, '设置列表获取成功')
          }
        }).catch(error => res.sendResult(null, 404, '分组不存在'))
    },
    byName: (req, res, next) => {
      Option.getListByName(req.params.name)
        .then(data => {
          // 输出的是提取的用户数据，敏感数据过滤掉
          res.sendResult(filter.extractOption(data), 200, '设置获取成功')
        }).catch(error => res.sendResult(null, 404, '字段名称不存在'))
    },
    byId: (req, res, next) => {
      Option.getListById(req.params.id)
        .then(data => {
          // 输出的是提取的用户数据，敏感数据过滤掉
          res.sendResult(filter.extractOption(data), 200, '设置获取成功')
        }).catch(error => res.sendResult(null, 404, '设置不存在'))
    }
  },
  delete: (req, res, next) => {
    Option.deleteOption(req.params.id)
      .then(data => {
        if (data.n === 0) {
          res.sendResult(null, 404, '设置不存在')
        } else {
          res.sendResult(null, 204, '设置删除成功')
        }
      }).catch(error => res.sendResult(null, 500, error.message))
  },
  put: (req, res, next) => {
    Option.putOption(req.params.id, req.body)
      .then(data => {
        console.log(data);
        if (data.n === 0) {
          res.sendResult(null, 404, '设置不存在')
        } else {
          res.sendResult(null, 200, '设置内容更新成功')
        }
      }).catch(error => res.sendResult(null, 500, error.message))
  }
}