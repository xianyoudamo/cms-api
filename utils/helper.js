/*
 * @Descripttion:
 * @version:
 * @Author: Damo
 * @Date: 2020-11-21 14:39:16
 * @LastEditors: Damo
 * @LastEditTime: 2020-11-24 10:52:00
 */
module.exports = {
  /**
   * 防注入提取提交的数据
   *
   * @param {object} params
   * @param {array} fields
   * @return {object} result{value:'',error:''}
   */
  extractParams(params, fields) {
    const result = {
      value: {},
    }
    // 防注入正则
    const reg = /(\$ne|\$gte|\$gt|\$lt|\$lte|\$in|\$nin|\$exists|\$where|tojson|\=\=|db\.)/gim
    if (typeof params === 'object' && Object.keys(params).length !== 0) {
      for (const key in params) {
        if (fields.includes(key)) {
          let item = params[key]
          if (typeof item === 'string') {
            item = item.replace(reg, '')
          }
          result.value[key] = item
        }
      }
    } else {
      result.error = '参数错误'
    }
    return result
  },
  /**
   * @param {number} num1
   * @param {number} num2
   * @return {number} 返回number类型数字相加的和
   */
  floatSum(num1, num2) {
    const num1Arr = num1.toString().split('.'),
      num2Arr = num2.toString().split('.')

    let num1Integer = num1Arr[0] ? num1Arr[0] : '0',
      num2Integer = num2Arr[0] ? num2Arr[0] : '0',
      num1Decimal = num1Arr[1] ? num1Arr[1] : '0',
      num2Decimal = num2Arr[1] ? num2Arr[1] : '0',
      num1DecimalLenth = num1Decimal.length,
      num2DecimalLenth = num2Decimal.length
    let sumIteger = fillZero(num1Integer) + fillZero(num2Integer)
    let decimalLength = Math.max(num1DecimalLenth, num2DecimalLenth)
    let sumDicimal =
      fillZero(num1Decimal, Math.abs(num1DecimalLenth - decimalLength)) +
      fillZero(num2Decimal, Math.abs(num2DecimalLenth - decimalLength))

    let isCarry = sumDicimal.toString().length > decimalLength
    sumIteger = isCarry ? sumIteger + 1 : sumIteger
    sumDicimal = isCarry ?
      sumDicimal - fillZero('1', decimalLength) :
      sumDicimal
    let sum = [sumIteger, sumDicimal]
    return Number(sum.join('.'))

    function fillZero(num, length) {
      let _length = length ? length : 0
      if (_length === 0) {
        return parseInt(num, 10)
      }
      const zero = []
      for (let index = 0; index < _length; index++) {
        zero.push(0)
      }
      return parseInt(num + zero.join(''), 10)
    }
  },
  /**
   * 获取请求的客户端ip
   * @param {object} req express中的request
   * @return {string} 客户端请求的ip地址
   */
  getClientIP(req) {
    return (
      req.headers['x-forwarded-for'] || // 判断是否有反向代理 IP
      req.connection.remoteAddress || // 判断 connection 的远程 IP
      req.socket.remoteAddress || // 判断后端的 socket 的 IP
      req.connection.socket.remoteAddress
    )
  }
}