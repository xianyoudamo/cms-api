/*
 * @Descripttion:
 * @version:
 * @Author: Damo
 * @Date: 2020-11-21 21:15:17
 * @LastEditors: Damo
 * @LastEditTime: 2020-11-21 21:15:18
 */
const bcrypt = require('bcrypt');
const salt = bcrypt.genSaltSync(10);
const encryption = (pwd) => {
  return bcrypt.hashSync(pwd, salt);
}
const comparison = (pwd, pwded) => {
  return bcrypt.compareSync(pwd, pwded);
}
module.exports = {
  encryption,
  comparison
}