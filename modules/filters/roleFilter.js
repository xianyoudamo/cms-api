/*
 * @Descripttion:
 * @version:
 * @Author: Damo
 * @Date: 2020-11-22 18:00:00
 * @LastEditors: Damo
 * @LastEditTime: 2020-11-24 16:39:38
 */
// 导入助手函数
const helper = require('../../utils/helper')
// 数据校验规则
const schema = require('../validate/roleValidate')
const fields = [
  'id',
  '_id',
  'role_name',
  'role_info',
  'authority'
]
const getFeild = [
  '_id',
  'role_name',
  'role_info',
  'authority'
]

function putFilter(req) {
  // 过滤动态参数
  const params = req.params
  const handleResult = helper.extractParams(params, fields)
  return handleResult
}
module.exports = {
  extractRole: (data) => {
    return helper.extractParams(data, getFeild).value
  },
  post: (req, res, next) => {
    // 提取过滤字段
    const params = req.body
    const handleResult = helper.extractParams(params, fields)
    if (handleResult.error) {
      return res.sendResult(null, 400, handleResult.error)
    }
    // 校验提交的字段
    const schemaResult = schema.postSchema.validate(handleResult.value)
    if (schemaResult.error) {
      return res.sendResult(null, 422, schemaResult.error.message)
    }
    req.body = handleResult.value
    next()
  },
  get: (req, res, next) => {
    // 提取过滤字段
    const result = putFilter(req)
    if (result.error) {
      return res.sendResult(null, 400, result.error)
    }
    next()
  },
  put: (req, res, next) => {
    // 提取过滤url中字段
    const result = putFilter(req)
    if (result.error) {
      return res.sendResult(null, 400, result.error)
    }
    req.params = result.value
    // 过滤put提交的字段
    const params = req.body
    const handleResult = helper.extractParams(params, fields)
    if (handleResult.error) {
      return res.sendResult(null, 400, handleResult.error)
    }
    // 校验提交的字段
    const schemaResult = schema.putSchema.validate(handleResult.value)
    if (schemaResult.error) {
      return res.sendResult(null, 422, schemaResult.error.message)
    }
    req.body = handleResult.value
    next()
  },
  delete: (req, res, next) => {
    // 提取过滤字段
    const result = putFilter(req)
    if (result.error) {
      return res.sendResult(null, 400, result.error)
    }
    next()
  }
}