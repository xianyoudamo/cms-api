/*
 * @Descripttion:
 * @version:
 * @Author: Damo
 * @Date: 2020-11-21 23:19:36
 * @LastEditors: Damo
 * @LastEditTime: 2020-11-24 08:55:52
 */
// 导入助手函数
const helper = require('../../utils/helper')
// 数据校验规则
const schema = require('../validate/userValidate')
const fields = [
  'user_name',
  'user_pwd',
  'author_name',
  'role_id',
  'user_email',
  'user_phone',
  'user_gravatar',
  'user_display',
  'login_ip',
  'login_time',
  'pagenum',
  'pagesize',
  'query',
  'id'
]
const getFields = [
  '_id',
  'user_name',
  'author_name',
  'role_id',
  'user_email',
  'user_phone',
  'user_gravatar',
  'user_display',
  'login_ip',
  'login_time'
]
function putFilter(req) {
  // 过滤动态参数
  const params = req.params
  const handleResult = helper.extractParams(params, fields)
  return handleResult
}
module.exports = {
  extractUser: (data) => {
    return helper.extractParams(data, getFields).value
  },
  post: (req, res, next) => {
    // 提取客户端提交的字段，防注入
    const params = req.body
    const handleResult = helper.extractParams(params, fields)
    if (handleResult.error) {
      return res.sendResult(null, 400, handleResult.error)
    }
    // 处理数据
    handleResult.value.user_display =
      params['user_display'] && params['user_display'] == 'on' ? 1 : 0
    handleResult.value.author_name =
      params['author_name'] && params['author_name'] != '' ?
      params['author_name'] : '小编'
    // 校验数据
    const schemaResult = schema.postSchema.validate(handleResult.value)
    if (schemaResult.error) {
      return res.sendResult(null, 422, schemaResult.error.message)
    }
    req.body = handleResult.value
    next()
  },
  list: (req, res, next) => {
    // 过滤url中的参数
    const params = req.query
    const handleResult = helper.extractParams(params, fields)
    if (handleResult.error) {
      return res.sendResult(null, 400, handleResult.error)
    }
    req.query = handleResult.value
    next()
  },
  get: (req, res, next) => {
    // 过滤动态参数
    const result = putFilter(req)
    if (result.error) {
      return res.sendResult(null, 400, result.error)
    }
    req.params = result.value
    next()
  },
  put: (req, res, next) => {
    // 过滤动态参数
    const result = putFilter(req)
    if (result.error) {
      return res.sendResult(null, 400, result.error)
    }
    req.params = result.value
    // 过滤put提交的字段
    const params = req.body
    const handleResult = helper.extractParams(params, fields)
    if (handleResult.error) {
      return res.sendResult(null, 400, handleResult.error)
    }
    // 校验数据
    const schemaResult = schema.putSchema.validate(handleResult.value)
    if (schemaResult.error) {
      return res.sendResult(null, 422, schemaResult.error.message)
    }
    req.body = handleResult.value
    next()
  },
  delete: (req, res, next) => {
    // 过滤动态参数
    const result = putFilter(req)
    if (result.error) {
      return res.sendResult(null, 400, result.error)
    }
    req.params = result.value
    next()
  },
  stateAndRole: (req, res, next) => {
    // 过滤动态参数
    const result = putFilter(req)
    if (result.error) {
      return res.sendResult(null, 400, result.error)
    }
    req.params = result.value
    // 过滤用户状态
    const params = req.body
    const handleResult = helper.extractParams(params, fields)
    if (handleResult.error) {
      return res.sendResult(null, 400, handleResult.error)
    }
    // 校验数据
    const schemaResult = schema.putSchema.validate(handleResult.value)
    if (schemaResult.error) {
      return res.sendResult(null, 422, schemaResult.error.message)
    }
    req.body = handleResult.value
    next()
  }
}