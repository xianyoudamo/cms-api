/*
 * @Descripttion:
 * @version:
 * @Author: Damo
 * @Date: 2020-11-24 17:49:26
 * @LastEditors: Damo
 * @LastEditTime: 2020-11-24 23:16:04
 */
// 导入助手函数
const helper = require('../../utils/helper')
// 数据校验规则
const schema = require('../validate/optionValidate')
const fields = [
  'option_name',
  'option_info',
  'group_id',
  'group_info',
  'option_type',
  'option_value',
  'remove',
  'gid',
  'name',
  'type',
  'id'
]
const getFields = [
  'option_name',
  'option_info',
  'group_id',
  'group_info',
  'option_type',
  'option_value',
  '_id'
]

function paramsFilter(req) {
  // 过滤动态参数
  const params = req.params
  const handleResult = helper.extractParams(params, fields)
  return handleResult
}
module.exports = {
  extractOption: (data) => {
    return helper.extractParams(data, getFields).value
  },
  post: (req, res, next) => {
    // 提取客户端提交的字段，防注入
    const params = req.body
    const handleResult = helper.extractParams(params, fields)
    if (handleResult.error) {
      return res.sendResult(null, 400, handleResult.error)
    }
    // 校验数据
    const schemaResult = schema.postSchema.validate(handleResult.value)
    if (schemaResult.error) {
      return res.sendResult(null, 422, schemaResult.error.message)
    }
    req.body = handleResult.value
    next()
  },
  list: (req, res, next) => {
    // 提取客户端提交的字段，防注入
    const params = req.query
    const handleResult = helper.extractParams(params, fields)
    if (handleResult.error) {
      return res.sendResult(null, 400, handleResult.error)
    }
    req.query = handleResult.value
    next()
  },
  get: (req, res, next) => {
    // 提取客户端提交的字段，防注入
    const result = paramsFilter(req)
    if (result.error) {
      return res.sendResult(null, 400, result.error)
    }
    req.params = result.value
    next()
  },
  delete: (req, res, next) => {
    // 提取客户端提交的字段，防注入
    const result = paramsFilter(req)
    if (result.error) {
      return res.sendResult(null, 400, result.error)
    }
    req.params = result.value
    next()
  },
  put: (req, res, next) => {
    // 提取客户端提交的字段，防注入
    const result = paramsFilter(req)
    if (result.error) {
      return res.sendResult(null, 400, result.error)
    }
    req.params = result.value
    // 提取客户端提交的字段，防注入
    const params = req.body
    const handleResult = helper.extractParams(params, fields)
    if (handleResult.error) {
      return res.sendResult(null, 400, handleResult.error)
    }
    // 校验数据
    const schemaResult = schema.putSchema.validate(handleResult.value)
    if (schemaResult.error) {
      return res.sendResult(null, 422, schemaResult.error.message)
    }
    req.body = handleResult.value
    next()
  }
}