/*
 * @Descripttion:
 * @version:
 * @Author: Damo
 * @Date: 2020-11-22 19:48:01
 * @LastEditors: Damo
 * @LastEditTime: 2020-11-22 20:49:50
 */
const model = require('./database/schema/users')
const {
  comparison
} = require('../utils/encryption')
const {
  loginSchema: schema
} = require('./validate/userValidate')
const fileds = [
  'user_name',
  '_id',
  'author_name',
  'role_id',
  'user_email',
  'user_phone',
  'user_gravatar',
  'user_display'
]
const helper = require('../utils/helper')
module.exports = {
  dologin: async (username, pwd, callback) => {
    if ((username && username.trim().length > 0) && (pwd && pwd.trim().length > 0)) {
      const schemaResult = schema.validate({
        user_name: username,
        user_pwd: pwd
      })
      if (schemaResult.error) {
        return callback && callback(schemaResult.error.message, null)
      }
      let user
      try {
        user = await model.findOne({
          user_name: username
        })
      } catch (error) {
        console.log(error.message)
      }
      if (user && user.user_display === 1) {
        const isCorrect = comparison(pwd, user.user_pwd)
        if (isCorrect) {
          return callback && callback(null, helper.extractParams(user, fileds).value)
        } else {
          return callback && callback('用户名或密码错误', null)
        }
      } else {
        return callback && callback('用户名、密码错误或者用户被禁用', null)
      }
    } else {
      return callback && callback('用户名或密码错误', null)
    }
  }
}