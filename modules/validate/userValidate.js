/*
 * @Descripttion:
 * @version:
 * @Author: Damo
 * @Date: 2020-11-21 21:06:44
 * @LastEditors: Damo
 * @LastEditTime: 2020-11-24 21:00:16
 */
const Joi = require('joi')
module.exports = {
  postSchema: Joi.object({
    user_name: Joi.string().regex(/^[a-z][a-z0-9]{4,15}$/).required().error(new Error('用户名以字母开头，最短5个字符，最长16个字符')),
    user_pwd: Joi.string().regex(/^[A-z0-9-_]{8,32}$/).required().error(new Error('密码只能是大小写字母数字-_，且不少于8位')),
    author_name: Joi.string().regex(/^[\u4E00-\u9FA5]{2,20}$/).error(new Error('笔名只能是汉字')),
    role_id: Joi.string().required().length(24).error(new Error('请填写角色id')),
    user_email: Joi.string().email().error(new Error('请填写正确的邮箱')),
    user_phone: Joi.string().regex(/^(13[0-9]|14[5|7]|15[0|1|2|3|4|5|6|7|8|9]|1[7|8|9][0|1|2|3|5|6|7|8|9])\d{8}$/).error(new Error('填写正确的手机号码')),
    user_gravatar: Joi.string().regex(/^\/[A-z0-9-_\/]+\.(jpg|png|gif|jpeg)$/).error(new Error('图片不可用')),
    user_display: Joi.number().valid(0, 1),
    login_time: Joi.number(),
    login_ip: Joi.string()
  }),
  putSchema: Joi.object({
    user_name: Joi.string().regex(/^[a-z][a-z0-9]{4,15}$/).error(new Error('用户名以字母开头，最短5个字符，最长16个字符')),
    user_pwd: Joi.string().regex(/^[A-z0-9-_]{8,32}$/).error(new Error('密码只能是大小写字母数字-_，且不少于8位')),
    author_name: Joi.string().regex(/^[\u4E00-\u9FA5]{2,20}$/).error(new Error('笔名只能是汉字')),
    role_id: Joi.string().length(24).error(new Error('请填写角色id')),
    user_email: Joi.string().email().error(new Error('请填写正确的邮箱')),
    user_phone: Joi.string().regex(/^(13[0-9]|14[5|7]|15[0|1|2|3|4|5|6|7|8|9]|1[7|8|9][0|1|2|3|5|6|7|8|9])\d{8}$/).error(new Error('填写正确的手机号码')),
    user_gravatar: Joi.string().regex(/^\/[A-z0-9-_\/]+\.(jpg|png|gif|jpeg)$/).error(new Error('图片不可用')),
    user_display: Joi.number().valid(0, 1),
    login_time: Joi.number(),
    login_ip: Joi.string()
  }),
  loginSchema: Joi.object({
    user_name: Joi.string().regex(/^[a-z][a-z0-9]{4,15}$/).required().error(new Error('用户名以字母开头，最短5个字符，最长16个字符')),
    user_pwd: Joi.string().regex(/^[A-z0-9-_]{6,32}$/).required().error(new Error('密码只能是大小写字母数字-_'))
  })
}