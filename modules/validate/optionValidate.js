/*
 * @Descripttion:
 * @version:
 * @Author: Damo
 * @Date: 2020-11-24 17:49:43
 * @LastEditors: Damo
 * @LastEditTime: 2020-11-25 15:26:59
 */
const Joi = require('joi')
module.exports = {
  postSchema: Joi.object({
    option_name: Joi.string().regex(/^[a-z_]{4,15}$/).required().error(new Error('系统设置字段名称只能是小写字母和_，最短5个字符，最长16个字符')),
    option_info: Joi.string().regex(/^[\u4E00-\u9FA5a-z]+$/).required().error(new Error('系统设置字段描述只能是汉字和小写字母')),
    group_id: Joi.number().min(1).max(10).error(new Error('分组名称只能数字,从1到10')),
    group_info: Joi.string().regex(/^[\u4E00-\u9FA5a-z]+$/).error(new Error('分组描述只能是汉字和小写字母')),
    option_type: Joi.number().required().valid(1, 3, 4, 6, 7, 8).error(new Error('系统设置中的字段类型只能是数字1:upload,3:input,4:textarea,6:file,7:select,8:checkbox')),
    option_value: Joi.string()
  }),
  putSchema: Joi.object({
    option_value: Joi.string()
  })
}