/*
 * @Descripttion:
 * @version:
 * @Author: Damo
 * @Date: 2020-11-22 17:56:29
 * @LastEditors: Damo
 * @LastEditTime: 2020-11-24 16:19:00
 */
const Joi = require('joi')
module.exports = {
  postSchema: Joi.object({
    role_name: Joi.string().regex(/^[\u4E00-\u9FA5]{2,20}$/).required().error(new Error('角色名称只能是汉字')),
    role_info: Joi.string().error(new Error('角色描述必须是字符串')),
    authority: Joi.string().error(new Error('权限列表必须是字符串'))
  }),
  putSchema: Joi.object({
    role_name: Joi.string().regex(/^[\u4E00-\u9FA5]{2,20}$/).error(new Error('角色名称只能是汉字')),
    role_info: Joi.string().error(new Error('角色描述必须是字符串')),
    authority: Joi.string().error(new Error('权限列表必须是字符串'))
  })
}