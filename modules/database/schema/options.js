/*
 * @Descripttion:
 * @version:
 * @Author: Damo
 * @Date: 2020-11-24 17:49:09
 * @LastEditors: Damo
 * @LastEditTime: 2020-11-25 15:26:36
 */
const mongoose = require('mongoose')
module.exports = mongoose.model(
  'Option',
  new mongoose.Schema({
    option_name: {
      type: String,
      required: [true, '请填写系统设置字段名称'],
      unique: true,
      index: true
    },
    option_info: {
      type: String,
      required: [true, '请填写系统设置字段描述']
    },
    group_id: {
      type: Number,
      // ui中是tabs,从左到右依次为1-10
      default: 1
    },
    group_info: {
      type: String,
      default: ''
    },
    option_type: {
      type: Number,
      required: [true, '请填写字段类型'],
      // 1:upload,3:input,4:textarea,6:file,7:select,8:checkbox
      enum: [1, 3, 4, 6, 7, 8]
    },
    option_value: {
      type: String,
      default: ''
    },
    removed: {
      type: Number,
      default: 0,
      // 0:正常,1:软删除
      enum: [1, 0]
    }
  }, {
    collection: 'cy_options',
    // 不保留__v
    versionKey: false
  })
)