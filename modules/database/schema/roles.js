/*
 * @Descripttion:
 * @version:
 * @Author: Damo
 * @Date: 2020-11-21 19:13:28
 * @LastEditors: Damo
 * @LastEditTime: 2020-11-22 18:24:59
 */
const mongoose = require('mongoose')
module.exports = mongoose.model(
  'Role',
  new mongoose.Schema({
    role_name: {
      type: String,
      required: [true, '请填写角色名称'],
      unique: true
    },
    role_info: {
      type: String,
      default: ''
    },
    authority: {
      type: String,
      default: ''
    }
  }, {
    collection: 'cy_roles',
    // 不保留__v
    versionKey: false
  })
)