/*
 * @Descripttion:
 * @version:
 * @Author: Damo
 * @Date: 2020-11-21 19:13:34
 * @LastEditors: Damo
 * @LastEditTime: 2020-11-23 17:53:36
 */
const mongoose = require('mongoose');
const schema = new mongoose.Schema({
  user_name: {
    type: String,
    required: [true, '用户名必须'],
    unique: true
  },
  user_pwd: {
    type: String,
    required: [true, '密码必须']
  },
  author_name: {
    type: String,
    default: ''
  },
  role_id: {
    type: mongoose.SchemaTypes.ObjectId,
    required: [true, '角色id必填'],
    ref: 'Role'
  },
  user_email: {
    type: String,
    default: ''
  },
  user_phone: {
    type: String,
    default: ''
  },
  user_gravatar: {
    type: String,
    default: ''
  },
  user_display: {
    type: Number,
    //0为禁用，1为启用
    default: 1
  },
  login_time: {
    type: Number,
    default: +new Date()
  },
  login_ip: {
    type: String,
    default: ''
  }
}, {
  collection: 'cy_users',
  // 不保留__v
  versionKey: false
})
schema.index({
  user_name: 'text'
})
module.exports = mongoose.model('User', schema)