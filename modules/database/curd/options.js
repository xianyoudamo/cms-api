/*
 * @Descripttion:
 * @version:
 * @Author: Damo
 * @Date: 2020-11-24 17:49:02
 * @LastEditors: Damo
 * @LastEditTime: 2020-11-25 15:49:24
 */
// mongodb数据模型
const model = require('../schema/options')
/**
 * 添加设置
 * @param {object} body post提交并且处理过的字段
 * @return {promise} 返回添加的设置信息
 */
exports.addOption = body => {
  return new Promise((resolve, reject) => {
    model.findOne({
        option_name: body.option_name
      })
      .then(async record => {
        // 判断是否存在相同的系统设置字段名称
        if (record) {
          return reject(new Error('字段名称不可用'))
        }
        const data = await model.create(body)
        resolve(data)
      })
      .catch(error => reject(error))
  })
}

/**
 * 查询所有设置信息
 * @return {promise}
 */
exports.getList = () => model.find({
  removed: 0
})
/**
 * 根据group_id获取设置信息
 * @param {string} gid
 * @return {promise} 返回设置信息
 */
exports.getListByGid = gid => model.find({
  group_id: gid,
  removed: 0
})

/**
 * 根据name获取设置信息
 * @param {string} name
 * @return {promise} 返回设置信息
 */
exports.getListByName = name => model.findOne({
  option_name: name,
  removed: 0
})
/**
 * 根据id获取设置信息
 * @param {string} id
 * @return {promise} 返回设置信息
 */
exports.getListById = id => model.findOne({
  _id: id,
  removed: 0
})
/**
 * 根据id删除设置
 * @param {string} id
 * @return {promise} 删除成功的信息
 */
exports.deleteOption = id => model.updateOne({
  _id: id,
  removed: 0
}, {
  removed: 1
})
/**
 * 根据id更新设置内容
 * @param {string} id
 * @param {object} body
 * @return {promise} 返回成功的信息
 */
exports.putOption = (id, body) => model.updateOne({
  _id: id,
  removed: 0
}, body)