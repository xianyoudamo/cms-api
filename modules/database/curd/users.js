/*
 * @Descripttion:
 * @version:
 * @Author: Damo
 * @Date: 2020-11-21 19:39:29
 * @LastEditors: Damo
 * @LastEditTime: 2020-11-24 23:33:29
 */
// mongodb数据模型
const model = require('../schema/users')
const pagination = require('mongoose-sex-page')
// 密码hash加密
const {
  encryption
} = require('../../../utils/encryption')
/**
 * 添加用户
 * @param {object} body post提交并且处理过的字段
 * @return {promise} 返回查询的结果
 */
exports.addUser = body => {
  return new Promise((resolve, reject) => {
    model.findOne({
        user_name: body.user_name
      })
      .then(async record => {
        // 判断是否存在用户名
        if (record) {
          return reject(new Error('用户名不可用'))
        }
        // 加密提交的密码并添加到数据库
        body.user_pwd = encryption(body.user_pwd)
        const data = await model.create(body)
        return resolve(data)
      })
      .catch(error => reject(error))
  })
}
/**
 * 获取用户列表
 * @param {object} option
 * {
 *    show:显示的页码数
 *    pagenum:当前页面
 *    pagesize:每页显示的条数
 *    query:要查询的关键词
 * }
 * @return {promise} 返回用户数组
 */
exports.getUsers = (option) => {
  const query = option.query
  const search = query ? {
    $text: {
      $search: query
    }
  } : {}
  return pagination(model).find(search).populate([{
    path: 'role_id',
    select: 'role_name role_info -_id'
  }]).display(option.show).page(option.pagenum).size(option.pagesize).exec()
}
/**
 * 根据id获取用户信息
 * @param {string} id
 * @return {promise} 用户的信息
 */
exports.getUser = id => model.findById(id).populate([{
  path: 'role_id',
  select: 'role_name role_info -_id'
}])
/**
 * 根据id删除用户
 * @param {string} id
 * @return {promise} 删除的用户信息
 */
exports.deleUser = id => model.deleteOne({
  _id: id
})

/**
 * 根据id更新用户信息
 * @param {string} id
 * @param {object} body
 * @return {promise} 返回更新的用户信息
 */
exports.putUser = (id, body) => model.updateOne({
  _id: id
}, body)

/**
 * 根据id更新用户状态
 * @param {string} id
 * @param {object} state
 * @return {promise} 返回更新的用户信息
 */
exports.putState = (id, state) => model.updateOne({
  _id: id
}, state)

/**
 * 根据id分配用户角色
 * @param {string} id
 * @param {object} state
 * @return {promise} 返回更新的用户信息
 */
exports.putRole = (id, role) => model.updateOne({
  _id: id
}, role)