/*
 * @Descripttion:
 * @version:
 * @Author: Damo
 * @Date: 2020-11-21 22:43:12
 * @LastEditors: Damo
 * @LastEditTime: 2020-11-24 17:03:51
 */
// mongodb数据模型
const model = require('../schema/roles');
/**
 * 添加角色
 * @param {object} body
 * @return {promise} 返回添加成功后的结果
 */
exports.addRole = async (body) => {
  try {
    const record = await model.findOne({
      role_name: body.role_name
    })
    if (record) {
      throw new Error('角色名称不可用')
    }
    return await model.create(body)
  } catch (error) {
    throw new Error(error.message)
  }
}
/**
 * 获取角色列表
 * @return {promise}
 */
exports.getRoles = () => model.find()

/**
 * 获取角色
 * @param {string} id 角色的_id
 * @return {promise} 返回获取的角色的信息
 */
exports.getRole = id => model.findById(id)
/**
 * 更新角色信息
 * @param {string} id
 * @param {object} body
 * @return {promise} 返回更新成功的结果
 */
exports.putRole = (id, body) => model.updateOne({
  _id: id
}, body)

/**
 * 删除角色
 * @param {string} id 角色的_id
 * @return {promise} 返回删除成功的信息
 */
exports.deleteRole = id => model.deleteOne({
  _id: id
})