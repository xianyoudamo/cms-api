/*
 * @Descripttion:
 * @version:
 * @Author: Damo
 * @Date: 2020-11-21 21:54:25
 * @LastEditors: Damo
 * @LastEditTime: 2020-11-24 22:04:12
 */
const express = require('express');
const api = express.Router();

// user相关的api
api.use('/users', require('./users'));

//role相关api
api.use('/roles', require('./roles'));

// option相关api
api.use('/options', require('./options'));
module.exports = api;