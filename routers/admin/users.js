/*
 * @Descripttion:
 * @version:
 * @Author: Damo
 * @Date: 2020-11-21 21:41:13
 * @LastEditors: Damo
 * @LastEditTime: 2020-11-23 22:47:44
 */
const express = require('express')
const user = express.Router()
const userController = require('../../controllers/userController')
const userFilter = require('../../modules/filters/userFilter')
// 添加用户api
user.post('/', userFilter.post, userController.add.post)
// 获取用户列表
user.get('/', userFilter.list, userController.get.list)
// 通过id获取用户信息
user.get('/:id', userFilter.get, userController.get.user)
// 通过id更新用户信息
user.put('/:id', userFilter.put, userController.put.user)
// 通过id删除用户
user.delete('/:id', userFilter.delete, userController.delete)
// 通过id修改用户状态
user.put('/:id/state', userFilter.stateAndRole, userController.put.state)
// 通过id给用户分配角色
user.put('/:id/role', userFilter.stateAndRole, userController.put.role)

module.exports = user