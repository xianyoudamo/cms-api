/*
 * @Descripttion:
 * @version:
 * @Author: Damo
 * @Date: 2020-11-22 18:06:18
 * @LastEditors: Damo
 * @LastEditTime: 2020-11-24 15:50:49
 */
const express = require('express')
const role = express.Router()
const roleController = require('../../controllers/roleController')
const roleFilter = require('../../modules/filters/roleFilter')
//添加角色api
role.post('/', roleFilter.post, roleController.add.post)
// 获取角色列表
role.get('/', roleController.get.list)
// 根据角色id获取角色信息
role.get('/:id', roleFilter.get, roleController.get.user)
// 根据角色id更新角色信息
role.put('/:id', roleFilter.put, roleController.put)
// 删除角色
role.delete('/:id', roleFilter.delete, roleController.delete)
module.exports = role