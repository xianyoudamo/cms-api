/*
 * @Descripttion:
 * @version:
 * @Author: Damo
 * @Date: 2020-11-24 17:48:47
 * @LastEditors: Damo
 * @LastEditTime: 2020-11-25 15:14:20
 */
const express = require('express')
const option = express.Router()
const optionController = require('../../controllers/optionController')
const optionFilter = require('../../modules/filters/optionFilter')
// 添加系统设置
option.post('/', optionFilter.post, optionController.post)
// 获取全部系统设置(query)type=[list|tree],list是并列的，tree按分组
option.get('/', optionFilter.list, optionController.get.list)
// 根据分组数字获取系统设置列表
option.get('/:gid/group', optionFilter.get, optionController.get.group)
// 通过系统设置字段名称获取字段信息
option.get('/:name/field', optionFilter.get, optionController.get.byName)
// 通过id获取系统设置
option.get('/:id', optionFilter.get, optionController.get.byId)
// 通过id删除的系统设置
option.delete('/:id', optionFilter.delete, optionController.delete)
// 通过id修改系统设置的值
option.put('/:id', optionFilter.put, optionController.put)

module.exports = option