/*
 * @Descripttion:
 * @version:
 * @Author: Damo
 * @Date: 2020-11-21 14:36:44
 * @LastEditors: Damo
 * @LastEditTime: 2020-11-23 20:39:52
 */
const express = require('express')
const app = express()
const bodyParser = require('body-parser')
const path = require('path')
const mongoose = require('mongoose')
const config = require('config')
// 解析post中提交的数据
app.use(bodyParser.urlencoded({
  extended: false
}))
app.use(bodyParser.json())

// 初始化统一响应机制
var resextra = require('./modules/resextra')
app.use(resextra)
//CORS跨域请求设置
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*')
  //请求方法大写，如果出现CROS不允许错误，改小写试下
  res.header('Access-Control-Allow-Methods', 'GET,POST,PUT,DELETE')
  //允许跨域客户端请求头设置项
  res.header('Access-Control-Allow-Headers', 'X-Requested-With, Authorization')
  res.header('Access-Control-Allow-Headers', 'Content-Type,Content-Length, Authorization, Accept,X-Requested-With')
  next()
})
//访问静态资源
app.use(express.static(path.join(__dirname, 'public')))

// 初始化 后台登录 passport 策略
admin_passport = require('./middleware/passport')
// 设置登录模块的登录函数衔接 passport 策略
admin_passport.setup(app, require('./modules/login').dologin)
// 设置 passport 登录入口点
app.use('/api/v1/admin/login', admin_passport.login)
// 设置 passport 验证路径
app.use('/api/v1/admin/*', admin_passport.tokenAuth)


//api的路由
app.use('/api/v1/admin', require('./routers/admin'))

app.listen(8888)
console.log('服务器启动...')
mongoose.set('useCreateIndex', true)
mongoose
  .connect(
    `mongodb://${config.get('database.dbuser')}:${config.get(
      'database.dbpwd'
    )}@${config.get('database.hostname')}:${config.get(
      'database.port'
    )}/${config.get('database.dbname')}`, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    }
  )
  .then(() => {
    console.log('数据库连接成功')
  })
  .catch((err) => {
    console.log('数据库连接失败', err)
  })